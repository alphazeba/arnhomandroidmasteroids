package alphazeba.aron.hommas.masteroids.gamebase;

import android.content.Context;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class ThreadedSurface extends SurfaceView{
    private Thread thread;
    private SurfaceHolder holder;
    private boolean running;
    private boolean cycleRunning;
    private Runnable r;
    protected float frameTime;
    private long timeFrameStart;
    private long lastTimeFrameStart;
    private long frameLength;
    private long onPauseOffset;
    private float frameTimeMax; //prevents super acceleration.

    protected boolean isPaused;


    public ThreadedSurface(Context context, AttributeSet attr){
        super(context,attr);

    }
    public ThreadedSurface(Context context){
        super(context);

    }

    private void goFullScreen(){
        //make it fullscreen.
        setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    protected void initialize(){
        //intialize variables and crap.
        holder = getHolder();

        // should remove this in the final version.
        if(holder!=null){
            Log.println(Log.INFO,"+holderStatus: ","holder was obtained.");
        }
        else{
            Log.println(Log.INFO,"-holderStatus: ","holder was not obtained.");
        }
        //////^^^^^^^^^^^^^^^///////////////
        cycleRunning = true;
        running = false;

        calculateFPS(60);
        timeFrameStart = System.currentTimeMillis();
        frameTime = 0;
        frameTimeMax = 1;

        //build and start the thread.
        r = new Runnable() {
            @Override
            public void run() {
                while(cycleRunning){
                    cycle();
                }
            }
        };
        onPauseOffset =timeFrameStart;
        startThread();
    }

    private void cycle(){
        //frame time is in milliseconds.
        //check if frame was too fast and a sleep is needed.
        long time =System.currentTimeMillis()-timeFrameStart;
        if(time<frameLength){
            SystemClock.sleep(frameLength-time);
        }

        //now update frame start times.
        lastTimeFrameStart = timeFrameStart;
        timeFrameStart = System.currentTimeMillis();

        //calculate frametime so that it can be used in the update function.
        frameTime = ((float)(timeFrameStart-lastTimeFrameStart))/1000;//frame time should be in seconds.


        if(frameTime < 0) {
            frameTime = 0; //prevents updates when frameTime is too large.
        }


        if(!isPaused) {
            update();
        }

        if(holder.getSurface().isValid()){
            Canvas canvas = holder.lockCanvas();
            mDraw(canvas);
            holder.unlockCanvasAndPost(canvas);
        }
    }

    protected void update(){

    }

    protected void mDraw(Canvas canvas){

    }

    public void startThread(){
        goFullScreen();

        cycleRunning=true;
        if(!running){
            running = true;
            thread = new Thread(r);
            thread.start();
        }

        //recalculates this so timing doesn't break on loadin

        onPauseOffset = System.currentTimeMillis() - onPauseOffset;
        lastTimeFrameStart+=onPauseOffset;
        timeFrameStart+=onPauseOffset;

    }

    public void stopThread(){

        cycleRunning=false;
        if(running){

            boolean retry = true;
            running = false;
            while(retry){
                try{
                    thread.join();
                    retry = false;
                }
                catch(Exception e){
                    Log.v("Xcept thrwn while join:",e.getMessage());
                }
            }
        }
        //for preventing break on reentry.
        onPauseOffset = System.currentTimeMillis();
    }

    public void calculateFPS(int fps){
        frameLength = (long)(1000.0f/fps);
    }

    public void pause(){
        isPaused = true;
    }

    public void unpause(){
        isPaused = false;
    }
}

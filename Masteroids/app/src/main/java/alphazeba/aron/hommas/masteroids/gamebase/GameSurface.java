package alphazeba.aron.hommas.masteroids.gamebase;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

import alphazeba.aron.hommas.masteroids.collisions.Collision;
import alphazeba.aron.hommas.masteroids.collisions.CollisionBulletAsteroid;
import alphazeba.aron.hommas.masteroids.collisions.CollisionBulletShip;
import alphazeba.aron.hommas.masteroids.managers.AsteroidManager;
import alphazeba.aron.hommas.masteroids.objects.Ship;
import alphazeba.aron.hommas.masteroids.objects.Stars;
import alphazeba.aron.hommas.masteroids.TouchControl;
import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.ai.StupidAI;
import alphazeba.aron.hommas.masteroids.managers.BulletManager;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class GameSurface extends ThreadedSurface {

    //can access frameTime as a float with frameTime
    //change fps with calculateFPS(int)

    //debug stuff
    private Paint paint;


    //game items.
        //camera  //TODO should make a more flexible camera that also allows, zoom and rotation
    private Vector2 cpos;

        //game objects
    private Ship ship;
    private Ship enemyShip;
    private Stars stars;

    private BulletManager bullets;
    private AsteroidManager asteroids;

    private StupidAI enemyai;

        //world size
    private Vector2 worldSize;

        //input
    private TouchControl input;

        //physical device info.
    private boolean screenMeasured;
    private int windowHeight;
    private int windowWidth;


    public GameSurface(Context context){
        super(context);

        windowHeight = 0;
        windowWidth = 0;

        worldSize = new Vector2(5000.f,5000.f);


        paint = new Paint();
        paint.setTextSize(50);
        paint.setColor(Color.GREEN);

        screenMeasured = false;
        bullets = new BulletManager(worldSize);
        asteroids = new AsteroidManager(worldSize);

        //initialize player ship
        input = new TouchControl();
        ship = new Ship(0,0,worldSize,input);
        ship.passGun(bullets);

        //camera
        cpos = new Vector2(ship.getPos().x - windowWidth/2,ship.getPos().y - windowHeight/2);

        //stars
        stars = new Stars(0,0);

        //initialize enemy ship
        enemyai = new StupidAI();
        enemyShip = new Ship(0,0,worldSize,enemyai);
        enemyai.passSelf(enemyShip);
        enemyai.setTarget(ship);
        enemyShip.passGun(bullets);
        enemyShip.setRotation(4);

        /////////////////
        //spawn a couple test Asteroids

        asteroids.spawn(20,520,300,3);
        asteroids.spawn(2500,2500,1200,6);


        super.initialize();
    }

    @Override
    protected void update(){
        ai();
/////////////////////////////////////
        enemyShip.update(frameTime);
        ship.update(frameTime);

        //camera
        cpos.x = ship.getPos().x - windowWidth/2;
        cpos.y = ship.getPos().y - windowHeight/2;


        bullets.update(frameTime);
        asteroids.update(frameTime);

////////////////////////////////////
        collision(frameTime);
    }

    private void collision(float frameTime){

        //ship collision.
         ship.collidesWith(frameTime,enemyShip);
        //astroids with ships
         asteroids.collidesWith(ship,frameTime);

        //bullets with ships
        Collision bulletShip = new CollisionBulletShip();
        bullets.collidesWith(ship,bulletShip,frameTime);
        bullets.collidesWith(enemyShip,bulletShip,frameTime);

        //bullets with asteroids
        bullets.collidesWith(asteroids,new CollisionBulletAsteroid(),frameTime);
    }

    private void ai(){
        enemyai.update(frameTime);
    }

    @Override
    protected void mDraw(Canvas canvas){

        //make a world canvas so that the camera can be disconnected.
        //blank out the canvas.
        canvas.drawColor(Color.argb(200,0,0,0));
        //canvas.drawRect(-cpos.x,-cpos.y,1000-cpos.x,1000-cpos.y,paint);

         //this block contains a bunch of test code for viewing variables and stuff.
        //canvas.drawCircle(100,100,80,paint);
        /*
        float t = 1/frameTime;
        canvas.drawText(Float.toString(t), 200,200,paint);
        canvas.drawText("x="+Float.toString(enemyShip.pos.x),200,250,paint);
        canvas.drawText("y="+Float.toString(enemyShip.pos.y),200,300,paint);
        canvas.drawText("rot="+Float.toString(enemyShip.rotation),200,350,paint);
        canvas.drawText("rov="+Float.toString(enemyShip.rotationV),200,400,paint);
        canvas.drawText("roa="+Float.toString(enemyShip.rotationAcceleration),200,450,paint);
        canvas.drawText("v.x="+Float.toString(enemyShip.velocity.x),200,500,paint);
        canvas.drawText("v.y="+Float.toString(enemyShip.velocity.y),200,550,paint);
        canvas.drawText("state="+enemyai.curState,200,600,paint);
        */
        //draw hit scores.
        canvas.drawText("mine: "+ Integer.toString(enemyShip.getHealth()),100,100,paint);
        canvas.drawText("theirs:"+Integer.toString(ship.getHealth()),windowWidth-300,100,paint);
        stars.draw(canvas,cpos);
        //drawObjects(canvas,cpos);
        //check if anything needs to be redrawn on the sides.


        int multiplier [] = {-1,0,1};
        for(int iy=0;iy<3;++iy) {
            for (int ix = 0; ix < 3; ++ix) {
                drawObjects(canvas, new Vector2(cpos.x + worldSize.x * multiplier[ix], cpos.y + worldSize.y * multiplier[iy]));
            }
        }
    }

    //this is where moveable objects are drawn so that they can be made to appear to wrap.
    private void drawObjects(Canvas canvas,Vector2 cpos){
        //draw the important stuff.
        ship.draw(canvas,cpos);
        enemyShip.draw(canvas,cpos);
        bullets.draw(canvas,cpos);

        asteroids.draw(canvas,cpos);
    }

    @Override
    public boolean onTouchEvent(MotionEvent me){
        input.update(me); //passes touch event over to touch control object.
        if(input.pausePressed()){
            if(isPaused){
                unpause();
            }else{
                pause();
            }
        }
        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        windowHeight = MeasureSpec.getSize(heightMeasureSpec);//masks are used to hid info in heightmeasuredspec.
        windowWidth =  MeasureSpec.getSize(widthMeasureSpec); //.getsize is the standard style of getting size.

        if(!screenMeasured){

            input.setWindow(windowWidth,windowHeight);
            ship.setXY(windowWidth/2,windowHeight/2);
            enemyShip.setXY(500,500);

            screenMeasured = true;
        }
        setMeasuredDimension(widthMeasureSpec,heightMeasureSpec);//obligated to call this each onMeasure event.
    }
}

package alphazeba.aron.hommas.masteroids;

/**
 * Created by arnHom on 1/13/2017.
 */

public class Quadratic {
    public float r1,r2;
    public boolean realAnswer;

    public boolean calculate(float a, float b, float c){
        if(a == 0){
            realAnswer = false;
            return false;
        }
        float q = b*b-4*a*c;
        if(q>=0){
            float sq = (float)Math.sqrt(q);
            float d  = 1/(a+a);
            r1 = (-b+sq)*d;
            r2 = (-b-sq)*d;
            realAnswer = true;
            return true;
        }
        realAnswer = false;
        return false;
    }

}

package alphazeba.aron.hommas.masteroids.collisions;

import alphazeba.aron.hommas.masteroids.objects.ArnObject;
import alphazeba.aron.hommas.masteroids.objects.Asteroid;

/**
 * Created by arnHom on 17/10/17.
 */

public class CollisionBulletAsteroid extends Collision{
    @Override
    public void collide(ArnObject lh, ArnObject rh) {
        ((Asteroid)rh).hitByBullet();//informs the asteroid that it has been hit by a bullet.
        lh.kill();//destroys the bullet that hit the asteroid
    }
}

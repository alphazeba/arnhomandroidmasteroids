package alphazeba.aron.hommas.masteroids;

/**
 * Created by HOMMASAS1 on 11/26/2016.
 */
public class Vector3 {

    public float x;
    public float y;
    public float z;

    public Vector3(){
        x=0;
        y=0;
        z=0;
    }

    public Vector3(float x, float y,float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(Vector3 v){
        x = v.x;
        y = v.y;
        z = v.z;
    }

    //outputs a rotated vector3 without touching the current vector3.
    public Vector3 rotate(float theta){
        Vector3 output = new Vector3();

        output.x = (float)(this.x*Math.cos(theta) - this.y*Math.sin(theta));
        output.y = (float)(this.x*Math.sin(theta) + this.y*Math.cos(theta));
        output.z = this.z;

        return output;
    }

    //rotates the current vector3 by theta.
    public void rerotate(float theta) {
        float tx = x;

        x = (float)(tx*Math.cos(theta) - y*Math.sin(theta));
        y = (float)(tx*Math.sin(theta) + y*Math.cos(theta));
    }

    public Vector3 multiply(float v){
        Vector3 output = new Vector3(this);
        output.x*=v;
        output.y*=v;
        output.z*=v;
        return output;
    }

    public void rescale(float v){
        this.x*=v;
        this.y*=v;
        this.z*=v;
    }

    public float dot(Vector3 v){
        return  (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
    }


}

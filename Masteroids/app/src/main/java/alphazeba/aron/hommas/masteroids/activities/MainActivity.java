package alphazeba.aron.hommas.masteroids.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import alphazeba.aron.hommas.masteroids.R;
import alphazeba.aron.hommas.masteroids.gamebase.ActualGame;
//import alphazeba.aron.hommas.masteroids.activities.TitleScreen;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void switchToTitle(View view) {
        Intent intent =  new Intent(this, TitleScreen.class);
        //intent.putExtra(string , basically anything i think)
        startActivity(intent);
    }

    public void startGame(View view){
        Intent intent = new Intent(this,ActualGame.class);
        startActivity(intent);
    }
}

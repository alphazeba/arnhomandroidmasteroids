package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import alphazeba.aron.hommas.masteroids.Vector2;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class Line2 {
    private Vector2 a;
    private Vector2 b;

    public Line2(float x, float y, float x2,float y2){
        a = new Vector2(x,y);
        b = new Vector2(x2,y2);
    }
    public Line2(Vector2 v,Vector2 v2){
        a = v;
        b = v2;
    }

    public Line2(Line2 l){
        a = l.a;
        b = l.b;
    }

    public void rotate(float theta){
        a=a.rotate(theta);
        b=b.rotate(theta);
    }

    public void scale(float v){
        a.rescale(v);
        b.rescale(v);
    }



    public void draw(Canvas c,Paint p,float x, float y){
        c.drawLine(a.x+x,a.y+y,b.x+x,b.y+y,p);
    }
}

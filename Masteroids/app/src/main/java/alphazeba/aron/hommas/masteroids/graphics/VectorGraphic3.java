package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Vector;

/**
 * Created by HOMMASAS1 on 11/26/2016.
 */

public class VectorGraphic3 {
    private Vector<Line3> line3;

    public VectorGraphic3(){
        line3 = new Vector<>();
    }

    public VectorGraphic3(VectorGraphic3 v){
        for(int i = 0 ; i < v.line3.size() ; ++i){
            this.addLine(new Line3(v.line3.elementAt(i)));
        }
    }

    public VectorGraphic3(VectorGraphic3 v,float theta, float scale){
        line3 = new Vector<>();
        for(int i = 0 ; i < v.line3.size() ; ++i){
            this.addLine(new Line3(v.line3.elementAt(i)));
            this.line3.elementAt(i).rotate(theta);
            this.line3.elementAt(i).scale(scale);
        }
    }

    public void addLine(Line3 l){
        line3.add(l);
    }

    public void draw(Canvas c , Paint p, float x , float y){
        for(int i = 0 ; i < line3.size() ; ++i){
            line3.elementAt(i).draw(c,p,x,y);
        }
    }

    public void calculateViewAngle(float theta){
        float vz = (float)Math.cos(theta);
        float vy = (float)Math.sin(theta);
        for(int i = 0 ; i < line3.size() ; ++i){
            line3.elementAt(i).setViewAngle(vz,vy);
        }
    }
}

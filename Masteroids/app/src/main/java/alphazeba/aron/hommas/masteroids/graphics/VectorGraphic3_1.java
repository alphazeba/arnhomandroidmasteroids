package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Vector;

import alphazeba.aron.hommas.masteroids.Vector3;

/**
 * Created by HOMMASAS1 on 12/22/2016.
 */
public class VectorGraphic3_1 {
    private Vector<Vector3> points;
    private Vector<AbstractLine> lines;
    private float viewAngleY;
    private float viewAngleZ;

    public VectorGraphic3_1(){
        points = new Vector<>();
        lines = new Vector<>();
        viewAngleY = 1;
        viewAngleZ = 0;
    }

    public VectorGraphic3_1(VectorGraphic3_1 v){
        this();

        for(int i = 0 ; i < v.points.size() ; ++i) {
            this.addPoint(v.points.elementAt(i));
        }
        for(int i = 0 ; i < v.lines.size() ; ++i) {
            this.addLine(v.lines.elementAt(i));
        }

        this.viewAngleY = v.viewAngleY;
        this.viewAngleZ = v.viewAngleZ;
    }

    public VectorGraphic3_1(VectorGraphic3_1 v,float theta, float scale){
        this(v);
        for(int i = 0 ; i < v.points.size() ; ++i){
            this.points.elementAt(i).rerotate(theta);
            this.points.elementAt(i).rescale(scale);
        }
    }

    public void addPoint(Vector3 v) {
        points.add(new Vector3(v));
    }

    public void addPoint(float x, float y , float z) {
        points.add(new Vector3(x,y,z));
    }

    public void addLine(AbstractLine l){
        lines.add(new AbstractLine(l));
    }

    public void addLine(int a, int b) {
        lines.add(new AbstractLine(a,b));
    }

    public void draw(Canvas c , Paint p, float x , float y){
        for(int i = 0 ; i < lines.size() ; ++i){
            lines.elementAt(i).draw(c,p,x,y,points,viewAngleY,viewAngleZ);
        }
    }

    public void calculateViewAngle(float theta){
        viewAngleZ = (float)Math.cos(theta);
        viewAngleY = (float)Math.sin(theta);
    }
}

package alphazeba.aron.hommas.masteroids.managers;

import android.graphics.Canvas;

import java.util.Iterator;
import java.util.Vector;

import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.collisions.Collision;
import alphazeba.aron.hommas.masteroids.objects.ArnObject;

/**
 * Created by arnHom on 17/10/17.
 */

public class ManagerBase {
    protected Vector<ArnObject> objects;
    protected Vector2 worldSize;

    public ManagerBase(){
        objects = new Vector<>();
        worldSize = null;
    }

    public ManagerBase(Vector2 worldSize){
        this();
        this.worldSize = worldSize;
    }

    public void update(float frameTime){
        for(int i= 0 ; i < objects.size() ; ){
            if(objects.elementAt(i).isAlive()){
                objects.elementAt(i).update(frameTime);
                ++i;
            }
            else{
                objects.remove(i);//do not increase i if deleting an item.
            }
        }
    }

    public void draw(Canvas c, Vector2 cpos){
        for(int i = 0 ; i < objects.size() ; ++i){
            objects.elementAt(i).draw(c,cpos);
        }
    }

    protected void add(ArnObject obj){
        objects.addElement(obj);
    }

    public void collidesWith(ArnObject other, float frameTime){
        for(int i = 0; i < objects.size(); ++i){
            objects.elementAt(i).collidesWith(frameTime,other);
        }
    }
    public void collidesWith(ManagerBase other, float frameTime){
        for(int i = 0; i < other.objects.size(); ++i){
            collidesWith(other.objects.elementAt(i),frameTime);
        }
    }
    public void collidesWithSelf(float frameTime){
        if(objects.size()<2){
            return;
        }
        for(int i1 = 0 ; i1 < objects.size() ; ++i1){
            for(int i2 = 1; i2 < objects.size() ; ++i2){
                if(i1!=i2){
                    objects.elementAt(i1).collidesWith(frameTime,objects.elementAt(i2));
                }
            }
        }
    }
    public void collidesWith(ArnObject other, Collision collision, float frameTime){
        for(int i = 0; i < objects.size(); ++i){
            if(objects.elementAt(i).collidesWith(frameTime,other)){
                collision.collide(objects.elementAt(i),other);
            }
        }
    }
    public void collidesWith(ManagerBase other, Collision collision, float frameTime){
        for(int i = 0; i < other.objects.size(); ++i){
            collidesWith(other.objects.elementAt(i),collision,frameTime);
        }
    }
    public void collidesWithSelf(Collision collision, float frameTime){
        if(objects.size()<2){
            return;
        }
        for(int i1 = 0 ; i1 < objects.size() ; ++i1){
            for(int i2 = 1; i2 < objects.size() ; ++i2){
                if(i1!=i2){
                    if(objects.elementAt(i1).collidesWith(frameTime,objects.elementAt(i2))){
                        collision.collide(objects.elementAt(i1),objects.elementAt(i2));
                    }
                }
            }
        }
    }

}

package alphazeba.aron.hommas.masteroids.objects;

import android.graphics.Color;

import java.util.Random;

import alphazeba.aron.hommas.masteroids.Input;
import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.Vector3;
import alphazeba.aron.hommas.masteroids.graphics.VectorGraphic3_1;
import alphazeba.aron.hommas.masteroids.managers.BulletManager;
import alphazeba.aron.hommas.masteroids.objects.ArnObject;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class Ship extends ArnObject {


    private float shotTimer;
    private Input input;
    private BulletManager gun;
    private boolean shot;
    private Random rand;

    private int health;



    public Ship(float x, float y, Vector2 worldSize, Input input){

        super(x,y,worldSize);

        buildImg();

        rand = new Random();

        rotationAcceleration = 5.0f;
        friction = 0.2f;
        rotationFriction = 0.5f;

        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);

        scale = 50;

        radius = scale/4*3;
        mass = 5;

        //set variable;
        this.input = input;

        rotation = 2*halfpi;
        rotationV = 0;

        shotTimer = 0;
        shot = false;

        calculateAcceleration(scale);

        health = 0;
    }

    public void passGun(BulletManager g){
        gun = g;
    }

    public void buildImg(){
        //build the img.
        Vector3 tip = new Vector3(0,1.5f,0);                //0
        Vector3 leftFlank = new Vector3(-0.2f,0.5f,-0.2f);  //1
        Vector3 rightFlank = new Vector3(0.22f,0.5f,-0.2f); //2
        Vector3 leftSide = new Vector3(-0.5f,-0.5f,0);      //3
        Vector3 rightSide = new Vector3(0.5f,-0.5f,0);      //4
        Vector3 back = new Vector3(0,-1.25f,-0.25f);        //5
        Vector3 top = new Vector3(0,-0.5f,-0.5f);           //6

        img.addPoint(tip);
        img.addPoint(leftFlank);
        img.addPoint(rightFlank);
        img.addPoint(leftSide);
        img.addPoint(rightSide);
        img.addPoint(back);
        img.addPoint(top);

        //left edge
        img.addLine(0,1);
        img.addLine(1,3);
        img.addLine(3,5);
        //right edge
        img.addLine(0,2);
        img.addLine(2,4);
        img.addLine(4,5);
        //middle spike
        //img.addLine(new Line3(top,back));
        //cross tip
        img.addLine(1,6);
        img.addLine(2,6);

        img = new VectorGraphic3_1(img,halfpi*3,1);
        //view angle
        img.calculateViewAngle(halfpi/2);
    }

    @Override
    public void update(float frameTime){



        //get input
        if(input.leftPressed()){
            rotationV-=rotationAcceleration*frameTime;
        }
        if(input.rightPressed()){
            rotationV+=rotationAcceleration*frameTime;
        }
        if(input.thrustPressed()){
            accelerate(frameTime);
        }


        if(shotTimer<=0){
            if(input.shootPressed()){
                shoot();
                shot = true;
            }
        }else{
            shotTimer-=frameTime;
            shot = false;
        }


        super.update(frameTime);
    }

    public void wallBounce(int windowWidth,int windowHeight){

        if(pos.x < 0){
            pos.x= 0 ;
            bounceX();
        }
        if(pos.x > windowWidth){
            pos.x = windowWidth;
            bounceX();
        }
        if(pos.y < 0){
            pos.y = 0;
            bounceY();
        }
        if(pos.y > windowHeight){
            pos.y= windowHeight;
            bounceY();
        }
    }

    public void bounceX(){
        velocity.x *=-0.2f;
    }

    public void bounceY(){
        velocity.y*=-0.2f;
    }

    private void shoot(){
        float spread = halfpi/10;
        float curSpread = rand.nextFloat() * spread - (spread/2);
        gun.shoot(pos.x, pos.y, rotation + curSpread, 3000, velocity, this);
        shotTimer = 0.1f; //use to be .01
    }

    public void hitByBullet(){
        ++health;
    }

    public int getHealth(){
        return health;
    }
}

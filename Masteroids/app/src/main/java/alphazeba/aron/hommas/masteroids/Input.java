package alphazeba.aron.hommas.masteroids;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class Input {

    protected int w;
    protected int h;


    protected boolean left,right,thrust,shoot;

    public Input() {
        reset();
        w=0;
        h=0;
    }

    public void setWindow(int w, int h){
        this.w = w;
        this.h = h;
    }

    protected void reset(){
        thrust = false;
        left = false;
        right = false;
        shoot = false;
    }

    public boolean leftPressed(){
        return left&&!right;
    }

    public boolean rightPressed(){
        return right&&!left;
    }

    public boolean thrustPressed(){
        return thrust;
    }

    public boolean shootPressed(){
        return shoot;
    }
}

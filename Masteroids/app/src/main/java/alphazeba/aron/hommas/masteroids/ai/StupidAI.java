package alphazeba.aron.hommas.masteroids.ai;

/**
 * Created by HOMMASAS1 on 12/22/2016.
 */
public class StupidAI extends ArtificialIntelligenceBase {

    public StupidAI(){
        super();
        setChase();

    }

    public void update(float frameTime){
        if(stateComplete){
            setChase();
        }
        super.update(frameTime);

        //shoot if pointed directly at the target.
        if(getTargetOffset() < self.halfpi /10){
            shootBurst(0.3f);
        }
    }

}

package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import alphazeba.aron.hommas.masteroids.Vector3;

/**
 * Created by HOMMASAS1 on 11/26/2016.
 */
public class Line3 {
    private Vector3 a;
    private Vector3 b;
    private float viewAngleZ = 0;
    private float viewAngleY = 1;

    public Line3(float x, float y,float z, float x2,float y2,float z2){
        a = new Vector3(x,y,z);
        b = new Vector3(x2,y2,z2);
    }
    public Line3(Vector3 v,Vector3 v2){
        a = v;
        b = v2;
    }

    public Line3(Line3 l){
        a = l.a;
        b = l.b;
        viewAngleZ = l.viewAngleZ;
        viewAngleY = l.viewAngleY;
    }

    public void rotate(float theta){
        a=a.rotate(theta);
        b=b.rotate(theta);
    }


    public void scale(float v){
        a.rescale(v);
        b.rescale(v);
    }

    public void draw(Canvas c, Paint p, float x, float y){
        c.drawLine(a.x+x,(a.y*viewAngleY)+(a.z*viewAngleZ)+y,b.x+x,(b.y*viewAngleY)+(b.z*viewAngleZ)+y,p);
    }

    public void setViewAngle(float vz,float vy){
        viewAngleZ = vz;
        viewAngleY = vy;
    }
}

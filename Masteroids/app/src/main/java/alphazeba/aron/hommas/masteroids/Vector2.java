package alphazeba.aron.hommas.masteroids;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class Vector2 {
    public float x;
    public float y;

    public Vector2(){
        x=0;
        y=0;
    }

    public Vector2(float x, float y){
        this.x = x;
        this.y = y;
    }

    public Vector2(Vector2 v){
        x = v.x;
        y = v.y;
    }

    public void setEqualTo(Vector2 other){
        this.x = other.x;
        this.y = other.y;
    }

    public Vector2 rotate(float theta){
        Vector2 output = new Vector2();

        output.x = (float)(this.x*Math.cos(theta) - this.y*Math.sin(theta));
        output.y = (float)(this.x*Math.sin(theta) + this.y*Math.cos(theta));

        return output;
    }

    public Vector2 multiply(float v){
        Vector2 output = new Vector2(this);
        output.x*=v;
        output.y*=v;
        return output;
    }

    public Vector2 divide(float v){
        Vector2 output = new Vector2(this);
        output.x /= v;
        output.y /= v;
        return output;
    }

    public void rescale(float v){
        this.x*=v;
        this.y*=v;
    }

    public Vector2 subtract(Vector2 v){
        return new Vector2(this.x-v.x,this.y-v.y);
    }

    public Vector2 add(Vector2 v) {
        return new Vector2(this.x+v.x,this.y+v.y);
    }

    public void reAdd(Vector2 v){
        this.x += v.x;
        this.y += v.y;
    }

    public void normalize(){
        float h = (float)Math.sqrt((x*x) + (y*y));
        x /= h;
        y /= h;
    }

    public float dot(Vector2 v){
        return  (this.x*v.x) + (this.y*v.y);
    }
}

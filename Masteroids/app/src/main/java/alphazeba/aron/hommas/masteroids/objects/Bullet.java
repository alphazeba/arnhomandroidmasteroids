package alphazeba.aron.hommas.masteroids.objects;

import android.graphics.Canvas;
import android.graphics.Color;

import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.graphics.VectorGraphic3_1;

/**
 * Created by HOMMASAS1 on 11/28/2016.
 */
public class Bullet extends ArnObject {

    private float lifeTime;

    private ArnObject owner;

    public Bullet(float x, float y, Vector2 worldSize, float dir, float spd, Vector2 vi, ArnObject owner){
        super(x,y,worldSize);

        this.owner = owner;

        scale = 10;
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(6);

        radius = scale/2;
        mass = 0.1f;

        rotation = dir;
        acceleration = spd;

        velocity.reAdd(vi);
        accelerate(1); //applies full acceleration to velocity. but just this once.

        //since bullets are lasers now, this is unnecessary.
        //buildImg();

        lifeTime = 1.5f;

    }

    public boolean collidesWith(float frameTime, ArnObject other){
        //do not collide with owner.
        if(owner == other){
            return false;
        }

        //if the bullet collides with something other than owner, it will lose its owner.
        if(super.collidesWith(frameTime,other)){
            owner = null;
            return true;
        }
        return false;
    }

    public void buildImg(){

        img.addPoint(0,5,0);     //0 tip
        img.addPoint(0,-2,0);    //1 bottom
        img.addPoint(-1,-3,0.5f);//2 left top
        img.addPoint(1,-3,0.5f); //3 right top

        img.addLine(0,2);
        img.addLine(0,3);
        img.addLine(2,1);
        img.addLine(3,1);

        img = new VectorGraphic3_1(img,halfpi*3,1);
        img.calculateViewAngle(halfpi/2);
    }

    public void draw(Canvas c,Vector2 cpos){
        c.drawLine(pos.x-cpos.x,pos.y-cpos.y,pos.x-lastPosDiff.x-cpos.x,pos.y-lastPosDiff.y-cpos.y,paint);
    }

    public void update(float frameTime){
        lifeTime -= frameTime;
        if(lifeTime <= 0) {
            this.kill();
        }

        super.update(frameTime);
    }

}

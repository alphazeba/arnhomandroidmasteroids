package alphazeba.aron.hommas.masteroids.managers;

import java.util.Random;
import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.objects.ArnObject;
import alphazeba.aron.hommas.masteroids.objects.Bullet;

/**
 * Created by HOMMASAS1 on 11/28/2016.
 */
public class BulletManager extends ManagerBase{
    Random rand;

    public BulletManager(){
        this(null);
    }

    public BulletManager(Vector2 worldSize){
        super(worldSize);
        rand = new Random();
    }

    public void shoot(float x, float y, float dir , float spd, Vector2 vi, ArnObject owner){
        add(new Bullet(x,y,worldSize,dir,spd,vi,owner));
    }
}

package alphazeba.aron.hommas.masteroids;

import android.view.MotionEvent;

import java.util.Vector;

/**
 * Created by HOMMASAS1 on 12/22/2016.
 */
public class TouchControl extends Input {
    private Vector<Finger> finger;

    private enum ControlModes{thrusters,lefty}
    ControlModes mode;

    private boolean pauseDown,pauseLast;

    public TouchControl(){
        super();

        finger = new Vector<>();
        mode = ControlModes.thrusters;

        pauseDown = false;
        pauseLast = false;
    }

    protected void reset(){
        super.reset();
        pauseLast = pauseDown;
        pauseDown = false;
    }

    public void update(MotionEvent me){

        switch(me.getActionMasked()){
            //add a finger to the screen.
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                Finger f = new Finger();
                f.id = me.getPointerId(me.getActionIndex());
                finger.add(f);
                break;

            //remove a finger from the screen.
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:
                int id = me.getPointerId(me.getActionIndex());
                for(int i = 0; i < finger.size() ; ++i){
                    if(finger.elementAt(i).id == id){
                        finger.remove(i);
                        break;
                    }
                }
                break;
        }

        //finger is properly set up.
        //now find what is being pressed.
        reset();
        switch (mode){
            case thrusters:
                for(int i = 0 ; i < finger.size() ; ++i){
                    int index = me.findPointerIndex(finger.elementAt(i).id);
                    if(me.getY(index)>h/2) {
                        if(me.getY(index)>h/4*3){
                            if (me.getX(index) < w / 2) {
                                right = true;
                            } else {
                                left = true;
                            }
                        }else{
                            shoot = true;
                        }
                    }
                    pauseCheck(me,index);
                }
                if(left&&right){
                    thrust = true;
                }
                break;
            case lefty:
                for(int i = 0; i < finger.size() ; ++i){
                    int index = me.findPointerIndex(finger.elementAt(i).id);
                    if(me.getX(index)>w/2){//is in fire button zone.
                        if(me.getY(index) > h/2){
                            shoot = true;
                        }
                    }
                    else{ //in directional control area.
                        if(me.getY(index) > h/4*3){ //larger is in left/right area.
                            if(me.getX(index) < w/4){
                                left = true;
                            }
                            else{
                                right = true;
                            }
                        }else{
                            thrust = true;
                        }
                    }
                    pauseCheck(me,index);
                }
                break;
        }
    }

    public boolean pausePressed(){
        return pauseDown && !pauseLast;
    }

    private void pauseCheck(MotionEvent me, int index){
        if (me.getY(index) < h / 4) {
            pauseDown = true;
        }
    }

}

package alphazeba.aron.hommas.masteroids.managers;

import java.util.Random;

import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.objects.Asteroid;

/**
 * Created by arnHom on 17/10/16.
 */

public class AsteroidManager extends ManagerBase {

    //TODO remove the extra todos inside arnobject

    //TODO make a universal object holder.

    Random rand;

    public AsteroidManager(){
        this(null);
    }

    public AsteroidManager(Vector2 worldSize){
        super(worldSize);
        rand = new Random();
    }

    public void spawn(float x, float y, float scale, int splits){
        add(new Asteroid(x,y,worldSize,scale,splits,this));
    }

    public void spawn(float x, float y, Vector2 vi, float scale, int splits){
        add(new Asteroid(x,y,worldSize,vi,300*scale*rand.nextFloat(),6.28f*rand.nextFloat(),scale,splits,this));
    }
}
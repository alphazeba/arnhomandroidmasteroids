package alphazeba.aron.hommas.masteroids.collisions;

import alphazeba.aron.hommas.masteroids.objects.ArnObject;
import alphazeba.aron.hommas.masteroids.objects.Ship;

/**
 * Created by arnHom on 17/10/17.
 */

public class CollisionBulletShip extends Collision{
    @Override
    public void collide(ArnObject lh, ArnObject rh) {
        ((Ship)rh).hitByBullet();
        //bullets are lasers now, rotation is irrelevant.
        //bullets.elementAt(i).getRotationV() = rand.nextFloat()*10-5;
    }
}

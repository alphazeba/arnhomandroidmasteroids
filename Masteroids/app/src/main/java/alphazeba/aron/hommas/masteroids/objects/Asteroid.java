package alphazeba.aron.hommas.masteroids.objects;

import android.graphics.Color;

import java.util.Random;

import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.managers.AsteroidManager;

/**
 * Created by arnHom on 17/10/16.
 */

public class Asteroid extends ArnObject{
    private int splits;
    private int health;
    private AsteroidManager manager;

    //this is the main constructor
    public Asteroid(float x, float y , Vector2 worldSize, Vector2 vi, float initialForce, float initialForceDir, float scale , int splits, AsteroidManager manager){
        super(x,y,worldSize);
        buildImg();

        this.manager = manager;
        this.splits = splits;
        this.health = Math.max((int)(scale/75),1);//minimum value of 1 health.
        this.velocity.reAdd(vi);

        this.scale = scale;

        this.paint.setColor(Color.YELLOW);
        this.paint.setStrokeWidth(4+scale/100);

        this.radius = scale/2;

        this.mass = 0.5f * scale;

        //give the asteroid its initial velocity
        this.applyForce(initialForce,initialForceDir);

        //give random rotation and spin.
        Random rand = new Random();
        this.rotation = 2*halfpi * rand.nextFloat();
        this.rotationV = (halfpi * rand.nextFloat() )- (halfpi/2);
    }

    public Asteroid(float x, float y , Vector2 worldSize, float scale, int splits, AsteroidManager manager){
        this(x,y,worldSize, new Vector2(),100,0,scale,splits,manager);
    }

    public void buildImg(){
        img.addPoint(-0.5f,0,0);    //0 west side
        img.addPoint(0.5f,0,0);     //1 east side
        img.addPoint(0,0.5f,0);     //2 north side
        img.addPoint(0,-0.5f,0);    //3 south side
        img.addPoint(0,0,0.5f);     //4 top side
        img.addPoint(0,0,-0.5f);    //5 bottom side.

        //top triangle
        img.addLine(0,4);
        img.addLine(1,4);
        img.addLine(2,4);
        img.addLine(3,4);
        //bottom triange
        img.addLine(0,5);
        img.addLine(1,5);
        img.addLine(2,5);
        img.addLine(3,5);
        //cross beams


        img.calculateViewAngle(halfpi/2);
    }

    //returns true if the asteroid is dead.
    public boolean hitByBullet(){
        --health;
        if(health > 0){
            return false;
        }
        //only runs if health is at or below 0.
        kill();
        //spawn a new smaller asteroid
        if(splits>0){
            for(int i =0 ; i < 2; ++i) {
                manager.spawn(this.pos.x, this.pos.y, this.velocity, this.scale * 0.66f, this.splits - 1);
            }
        }
        return true;
    }
}

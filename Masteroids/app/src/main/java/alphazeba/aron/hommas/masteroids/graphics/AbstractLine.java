package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Vector;

import alphazeba.aron.hommas.masteroids.Vector3;

/**
 * Created by HOMMASAS1 on 12/22/2016.
 */

/*

stores the position of the point within a vector of points.
this is so that the points position only needs to be calculated once no matter how many lines use the same point.
*/
public class AbstractLine {
    public int a;
    public int b;

    AbstractLine(int a, int b){
        this.a = a;
        this.b = b;
    }

    AbstractLine(AbstractLine al){
        this.a = al.a;
        this.b = al.b;
    }

    public void draw(Canvas c, Paint p, float x, float y, Vector<Vector3> points, float viewAngleY, float viewAngleZ){
        //gets the vectors it is going to draw the line between into va and vb.
        //then draws the line with the viewangle applied to it.
        Vector3 va = new Vector3(points.elementAt(a));
        Vector3 vb = new Vector3(points.elementAt(b));
        c.drawLine(va.x+x,(va.y*viewAngleY)+(va.z*viewAngleZ)+y,vb.x+x,(vb.y*viewAngleY)+(vb.z*viewAngleZ)+y,p);
    }
}

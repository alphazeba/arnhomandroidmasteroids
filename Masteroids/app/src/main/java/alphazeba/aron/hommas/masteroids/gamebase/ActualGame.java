package alphazeba.aron.hommas.masteroids.gamebase;

import android.app.Activity;
import android.os.Bundle;

import alphazeba.aron.hommas.masteroids.gamebase.GameSurface;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class ActualGame extends Activity {

    private GameSurface s;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        s = new GameSurface(this);
        setContentView(s);
    }

    @Override
    protected void onPause(){
        super.onPause();
        s.stopThread();
    }

    @Override
    protected void onResume(){
        super.onResume();
        s.startThread();
    }
}

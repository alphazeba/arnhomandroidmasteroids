package alphazeba.aron.hommas.masteroids.collisions;

import alphazeba.aron.hommas.masteroids.objects.ArnObject;

/**
 *
 * Created by arnHom on 17/10/17
 * this is a pure virtual collision type.  rather than using this collision type when nothing interesting
 * occurs during a collision, use the collidesWith function that does not take a collision type.
 *
 * this is a parent function to other collision types.
 * to make a new collision type simply extend this and write a public void collide function in order
 * to handle special events that occur during different collision types.
 */

public class Collision {
    public Collision(){}

    public void collide(ArnObject lh,ArnObject rh){};
}

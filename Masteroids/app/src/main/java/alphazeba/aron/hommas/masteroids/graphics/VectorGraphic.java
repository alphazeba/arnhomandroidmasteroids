package alphazeba.aron.hommas.masteroids.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Vector;

/**
 * Created by HOMMASAS1 on 11/25/2016.
 */
public class VectorGraphic {
    private Vector<Line2> line2;

    public VectorGraphic(){
        line2 = new Vector<>();
    }

    public VectorGraphic(VectorGraphic v){
        for(int i = 0 ; i < v.line2.size() ; ++i){
            this.addLine(new Line2(v.line2.elementAt(i)));
        }
    }

    public VectorGraphic(VectorGraphic v,float theta, float scale){
        line2 = new Vector<>();
        for(int i = 0 ; i < v.line2.size() ; ++i){
            this.addLine(new Line2(v.line2.elementAt(i)));
            this.line2.elementAt(i).rotate(theta);
            this.line2.elementAt(i).scale(scale);
        }
    }

    public void addLine(Line2 l){
        line2.add(l);
    }

    public void draw(Canvas c ,Paint p,float x , float y){
        for(int i = 0 ; i < line2.size() ; ++i){
            line2.elementAt(i).draw(c,p,x,y);
        }
    }
}

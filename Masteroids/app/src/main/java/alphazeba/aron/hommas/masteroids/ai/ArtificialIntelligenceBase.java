package alphazeba.aron.hommas.masteroids.ai;

import alphazeba.aron.hommas.masteroids.Input;
import alphazeba.aron.hommas.masteroids.objects.Ship;
import alphazeba.aron.hommas.masteroids.Vector2;

/**
 * Created by HOMMASAS1 on 12/22/2016.
 */
public class ArtificialIntelligenceBase extends Input {

    private enum  GoalState {none,chase,mirror,shoot}
    GoalState curState;
    boolean stateComplete;

    Ship target;
    Ship self;

    private float goalDirection;
    private boolean shootFlag;
    private float shootTimer;

    ArtificialIntelligenceBase(){
        super();

        curState = GoalState.none;
        stateComplete = true;
        self = null;
        target = null;
        goalDirection = 0;
    }

    public void passSelf(Ship s){
        self = s;
    }

    public void setTarget(Ship s){
        target = s;
    }

    public void update(float frameTime){
        reset();//reset all control flags.

        //handle different action states.
        switch (curState){
            case chase:
                chase();
                break;
            case mirror:
                mirror();
                break;
            case shoot:
                shoot();
                break;
            default:
                stateComplete = true;
        }

        //handle shooting
        if(shootFlag){
            shoot = true;
        }
        else if(shootTimer > 0){
            shoot = true;
            shootTimer -= frameTime;
        }

    }

    private void chase(){
        if(target == null){
            stateComplete = true;
            return;
        }
        //get relative velocity and direction to the target. relative velocity is added to target direction to decide where the ship will aim.
        Vector2 relativeVelocity = target.getVelocity().subtract(self.getVelocity());
        Vector2 targetDirection = target.getPos().subtract(self.getPos());//new Vector2(target.x-self.x,target.y-self.y);
        Vector2 goalVector = targetDirection.add(relativeVelocity);//relativeVelocity.add(targetDirection);





        goalDirection = calculateAngleDifference(self.getRotation(),goalVector);


        if(Math.abs(goalDirection) > self.halfpi/2) {
            if (goalDirection < 0) {
                left = true;
            } else if (goalDirection > 0) {
                right = true;
            }
        }else{
            thrust = true;
        }

    }

    private void mirror(){

        right = true;

        if(target.getPos().x  < self.getPos().x){
            shoot = true;
        }
    }

    private void shoot(){
        stateComplete = true;
    }

    protected void setChase(){
        curState = GoalState.chase;
        stateComplete = false;
    }

    protected void setMirror(){
        curState = GoalState.mirror;
        stateComplete = false;
    }

    protected void setShoot(){
        curState = GoalState.shoot;
        stateComplete = false;
    }

    protected void setNone(){
        curState = GoalState.none;
        stateComplete = true;
    }

    protected float getGoalOffset(){
        return Math.abs(goalDirection);
    }

    protected void shootBurst(float burstTime){
        shootTimer = burstTime;
    }

    protected float getTargetOffset(){
        //float targetDirection =  (float)Math.atan2(-target.x+self.x,target.y-self.y);
        //TODO fix this
            //while in the upper left enemy will only shoot when looking away
            //while in lower right enemy will shoot correctly.
            //shoots close but wrong when in the other 2 quadarnts.

        float targetDirection = calculateAngleDifference(self.getRotation(),target.getPos());

        return Math.abs(targetDirection);
    }

    private float calculateAngleDifference(float a , Vector2 v){
        float b = (float)Math.atan2(v.y,v.x);

        if(b  < 0){
            b = self.halfpi * 4 + b;
        }

        float output = b-a;

        if(output > self.halfpi*2){
            output = output - self.halfpi*4;
        }
        else if(output < -self.halfpi*2){
            output = self.halfpi*4 - output;
        }

        return output;
    }
}

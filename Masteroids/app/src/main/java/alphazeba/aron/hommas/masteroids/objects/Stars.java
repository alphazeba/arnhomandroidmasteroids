package alphazeba.aron.hommas.masteroids.objects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;
import java.util.Vector;

import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.Vector3;
import alphazeba.aron.hommas.masteroids.objects.ArnObject;

/**
 * Created by arnHom on 17/08/15.
 */

public class Stars extends ArnObject {

    private class Star{
        Vector3 pos;
        Vector3 lastActual;

        Star(float x, float y, float z){
            pos = new Vector3(x,y,z);
            lastActual = new Vector3(pos);
        }

      public void draw(Canvas c, Vector2 cpos, Paint paint){
          c.drawCircle(pos.x-cpos.x*pos.z,pos.y-cpos.y*pos.z,1.f,paint);
          c.drawLine(pos.x-cpos.x*pos.z,pos.y - cpos.y*pos.z,lastActual.x,lastActual.y,paint);
          lastActual.x = pos.x-cpos.x*pos.z;
          lastActual.y = pos.y-cpos.y*pos.z;
      }

    };

    private Vector<Star> starVector;

    public Stars(float x,float y){
        super(x,y,null);

        scale = 10;
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(2);

        starVector = new Vector<>();
        for(int i =0 ; i < 500; ++i){
            Random r = new Random();
            starVector.add(new Star(r.nextInt(7200),r.nextInt(12800),r.nextFloat()));
        }
    }

    public void draw(Canvas c, Vector2 cpos){
        //c.drawLine(pos.x-cpos.x,pos.y-cpos.y,lastPos.x-cpos.x,lastPos.y-cpos.y,paint);
        for(int i= 0 ; i<starVector.size() ; ++i){
            starVector.elementAt(i).draw(c,cpos,paint);
        }
    }
}

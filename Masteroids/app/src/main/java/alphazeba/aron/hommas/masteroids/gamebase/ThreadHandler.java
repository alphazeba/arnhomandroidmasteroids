package alphazeba.aron.hommas.masteroids.gamebase;

import android.util.Log;

/**
 * Created by HOMMASAS1 on 11/23/2016.
 */
public class ThreadHandler {

    private Thread thread;
    private boolean running;

    ThreadHandler() {
        thread = new Thread();
        running = false;
    }

    public void startThread(){
        if(!running){
            running = true;
            thread.start();
        }
    }

    public void stopThread(){

       if(running){

           boolean retry = true;
           running = false;
           while(retry){
               try{
                   thread.join();
                   retry = false;
               }
               catch(Exception e){
                    Log.v("Xcept thrwn while join:",e.getMessage());
               }
           }
       }
    }

    public Thread getThread(){
        return thread;
    }

}

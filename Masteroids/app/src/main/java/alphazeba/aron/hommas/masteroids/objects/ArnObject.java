package alphazeba.aron.hommas.masteroids.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import alphazeba.aron.hommas.masteroids.Quadratic;
import alphazeba.aron.hommas.masteroids.Vector2;
import alphazeba.aron.hommas.masteroids.graphics.VectorGraphic3_1;

/**
 * Created by HOMMASAS1 on 11/28/2016.
 */
public class ArnObject {

    // private VectorGraphic img;
    protected VectorGraphic3_1 img;
    protected Paint paint;
    protected float scale;

    protected float rotation;
    protected Vector2 pos;
    protected Vector2 lastPosDiff;

    protected Vector2 velocity;
    protected float rotationV;



    protected float acceleration;
    protected float rotationAcceleration = 5.0f;
    public float halfpi = 1.570796f;
    protected float friction = 0.2f;
    protected float rotationFriction = 0.5f;

    protected float radius;
    protected float mass;

    protected boolean alive;

    protected Vector2 worldSize;

    //TODO make a loadImg function that takes in a file.
    //the goal is to replace the BuildImg function that is currently being used.

    public ArnObject(){
        alive = true;
        worldSize = null;
        paint = new Paint();
        img = new VectorGraphic3_1();
        velocity = new Vector2();
        scale = 1;
        rotation = 0;
        pos = new Vector2();
        lastPosDiff = new Vector2();
        rotationV = 0;
        acceleration =0;
        rotationAcceleration = 0;
        friction = 0;
        rotationFriction = 0;
        radius = 0;
    }
    /* //TODO, temporarily commented out ot more easily find all of its uses.
    public ArnObject(float x,float y){
        this();
        this.pos.x = x;
        this.pos.y = y;
    }
    */

    public ArnObject(float x, float y, Vector2 worldSize){
        this();
        this.pos.x = x;
        this.pos.y = y;
        this.worldSize = worldSize;
    }

    public void calculateAcceleration(float v){
        acceleration = 20 * v;
    }

    public void update(float frameTime){
        if(alive){
            friction(frameTime);
            move(frameTime);
            wrap();
        }
    }

    private void friction(float frameTime){
        velocity.x -= velocity.x*(friction*frameTime);
        velocity.y -= velocity.y*(friction*frameTime);

        rotationV -= rotationV*(rotationFriction*frameTime);
    }

    protected void accelerate(float frameTime){
        velocity.x += (acceleration*frameTime)*Math.cos(rotation);
        velocity.y += (acceleration*frameTime)*Math.sin(rotation);
    }

    //using a rotation
    public void applyForce(float strength, float r){
        float a = strength / this.mass;
        velocity.x +=  a*Math.cos(r);
        velocity.y +=  a*Math.sin(r);
    }
    //using a normalized vector
    public void applyForce(float strength, Vector2 n){
        float a = strength / this.mass;
        velocity.add(n.multiply(a));
    }
    //using a vector2
    public void applyForce(Vector2 v){
        Vector2 va = v.divide(this.mass);
        velocity.add(va);
    }

    private void move(float frameTime){
        rotation += rotationV*frameTime;
        //wrap rotation
        rotation = rotation % (halfpi * 4);
        if(rotation < 0){
            rotation = (halfpi * 4) - rotation;
        }

        //stores the amount moved between frames.
        lastPosDiff.setEqualTo(velocity.multiply(frameTime));

        pos.reAdd(lastPosDiff);
    }

    public void draw(Canvas c,Vector2 cpos){
        if(alive){
            VectorGraphic3_1 drawable = new VectorGraphic3_1(img,rotation,scale);
            drawable.draw(c,paint,pos.x-cpos.x,pos.y-cpos.y);
        }
    }

    public Vector2 getXY(){
        return pos;
    }

    public void setXY(float x, float y){
        this.pos.x = x;
        this.pos.y = y;
    }

    protected void setRadius(float r){
        radius = r;
    }

    private boolean quadratic(float a, float b, float c, float r1, float r2){
        float q = b*b-4*a*c;
        if(q >=0){
            float sq = (float)Math.sqrt(q);
            float d  = 1/(a+a);
            r1 = (-b+sq)*d;
            r2 = (-b-sq)*d;
            return true;
        }
        return false;
    }

    public boolean collidesWith(float frameTime, ArnObject other){
        //TODO add in a "vicinity check" before running all this other collision code.
        //should then calculate average work saved to see if it adds ro removes effort.

        //object placements this frame.
        Vector2 A1 = new Vector2(this.pos);
        Vector2 B1 = new Vector2(other.pos);

        //object placements last frame.
        Vector2 A0 = A1.subtract(this.lastPosDiff);
        Vector2 B0 = B1.subtract(other.lastPosDiff);

        Vector2 AV = A1.subtract(A0);
        Vector2 BV = B1.subtract(B0);
        Vector2 DV = BV.subtract(AV);

        //difference last frame.
        Vector2 D0 = B0.subtract(A0);

        //difference this frame.
        Vector2 D1 = B1.subtract(A1);

        //sweep collision between frames.
        float minDistSquared = D0.dot(D0) - D0.dot(D1)/D1.dot(D1);

        float rSquared = (this.radius+other.radius);
        rSquared *= rSquared;

        float normalTime;


        //find the time of collision.
        if(D0.dot(D0)<=rSquared){
            normalTime = 0;
        }
        else {
            Quadratic quadratic = new Quadratic();
            if (quadratic.calculate(DV.dot(DV), 2 * DV.dot(D0), D0.dot(D0) - rSquared)) {
                if(quadratic.r2 < quadratic.r1){
                    //switch the answers.
                    float temp = quadratic.r2;
                    quadratic.r2 = quadratic.r1;
                    quadratic.r1 = temp;
                }
                //r1 is guaranteed the lesser
                if(quadratic.r1 < 0){ //r1 is illegal, check if r2 is also too small.
                    if(quadratic.r2 < 0){
                        return false; //both answers were illegal
                    }
                    //we now drop the smaller number because we now know that the larger one is at least not too small.
                    quadratic.r1 = quadratic.r2; // both answers are the same now because the other was illegal
                }
                //this will check the larger one as well in the event the smaller number was too small.
                //this is why r2 was put in r1.
                if(quadratic.r1 > 1){
                    return false; //the answer was too large.
                }
                normalTime = quadratic.r1;
            }
            else{
                return false;//exit, there was no answer to the quadratic equation, therefore, no answer.
            }
        }

        //AC and BC are A and C's position at the time of the collision.
        Vector2 AC = A0.add(AV.multiply(normalTime));  //TODO make sure normalTIme is max 1.
        Vector2 BC = B0.add(BV.multiply(normalTime));
        Vector2 DC = BC.subtract(AC);
        Vector2 normal = new Vector2(DC);
        normal.normalize();
        if(Float.isNaN(AC.x) || Float.isNaN(AC.y)){

            Log.i("collisionerror", Float.toString(normalTime));
        }


        //1dimensional representations of velocity along the collision normal.
        //their collision velocities.
        float ACV = this.velocity.dot(normal);
        float BCV = other.velocity.dot(normal);

        //calculate an elastic collision between 2 likely moving objects.
        float newACV = ( (this.mass-other.mass)*ACV + 2*other.mass*BCV ) / ( this.mass + other.mass );
        float newBCV = ( (other.mass-this.mass)*BCV + 2*this.mass*ACV ) / ( this.mass + other.mass );

        //this puts the result of the 1 dimensional collision velocities back into velocity, but we lose ambient velocity.

        //subtract original 1dimensional collision velcoity and then add on the new 1dimensional collision velocity.
        this.velocity  = this.velocity.subtract(normal.multiply(ACV)).add(normal.multiply(newACV));
        other.velocity = other.velocity.subtract(normal.multiply(BCV)).add(normal.multiply(newBCV));


        //set objects apart from one another.
            //find the center of the collision
        Vector2 collisionPoint = AC.add(DC.multiply(this.radius/(this.radius+other.radius)));

            //find positions away from the collision point based on each object's radius.
            //the extra 0.1f is added as a buffer to prevent recollision.
        Vector2 newAPos = collisionPoint.subtract(normal.multiply(this.radius+0.1f));
        Vector2 newBPos = collisionPoint.add(normal.multiply(other.radius+0.1f));


        //set the new positions.
        this.pos.setEqualTo(newAPos);
        other.pos.setEqualTo(newBPos);



        return true;
    }

    private void wrap(){
        if(worldSize == null){
            return; //don't do anything if there isn't a worldsize defined.
        }
        if(this.pos.x<0){
            this.pos.x += worldSize.x;
        }
        else if(this.pos.x>worldSize.x){
            this.pos.x -= worldSize.x;
        }
        if(this.pos.y<0){
            this.pos.y += worldSize.y;
        }
        else if(this.pos.y>worldSize.y){
            this.pos.y -= worldSize.y;
        }
    }

    public boolean isAlive(){
        return alive;
    }

    public void kill(){
        alive = false;
    }

    public Vector2 getPos(){
        return pos;
    }

    public Vector2 getVelocity(){
        return velocity;
    }

    public void setRotation(float r){
        rotation = r;
    }
    public float getRotation(){
        return rotation;
    }

    public float getRotationV() { return rotationV; }
}
